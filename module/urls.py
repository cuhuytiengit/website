"""website URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path,include
from django.urls import path
from module.Home.controller import HomeController
from module.Cart.controller import ControllerCart
from module.Login.controller import LoginController
from module.Checkout.controller import CheckoutController
from module.Payment.controller import PaymentController
from module.Register.controller import RegisterController

from django.conf.urls.static import static
from django.conf import settings
app_name = "module"
urlpatterns = [
    path('website_cosmetic/', HomeController.index, name ='home'),

    path('cart/', ControllerCart.index, name ='cart'),
    path('cart/', ControllerCart.delete_cart, name ='delete_cart'),
    path('login/', LoginController.login, name ='login'),
    path('checkout/', CheckoutController.checkout, name ='checkout'),
    path('payment/', PaymentController.payment, name ='payment'),
    path('register/', RegisterController.get_register, name ='get_register'),
    path('post_register/', RegisterController.post_register, name ='post_register'),



    
    

    path('admin/', admin.site.urls),
]+ static(settings.MEDIA_URL, document_root = settings.MEDIA_ROOT)
if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)