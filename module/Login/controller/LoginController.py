from contextlib import redirect_stderr
from django.template import loader
from django.shortcuts import redirect
from django.shortcuts import render
from django.http import HttpResponse
import requests,urllib3
import collections 
import datetime
import sys
import json
import pytz
from django.contrib import messages
from base64 import b64encode
import requests
if sys.version_info.major == 3 and sys.version_info.minor >= 10:
    from collections.abc import MutableMapping
else:
    from collections import MutableMapping
# from module.EndUser.model.ProductModelEndUser import ProductModelUser
from django.contrib.auth import authenticate, login



def login(request):

    if request.method == 'GET':

        if 'cart' in  request.session:
            product_get = json.loads(json.dumps(request.session.get('cart')))
            print(product_get)
            print(type(product_get))

            final_products_cart = {}
            for x in product_get.keys():
                if x != 'null':
                    final_products_cart[x] = request.session.get('cart')[x]

            ids = final_products_cart.keys()
            quantity = list(final_products_cart.values())
            print('test ids')
            print(ids)
            print('test quantity')
            print(quantity)
            sum_quantity = sum(quantity)
            sumquantity = sum_quantity
            print(sumquantity)
            products_list = []
            print(products_list)
            for x in ids:
                product = requests.get('http://192.168.1.25:5000/get-product-by-id/'+x).json()
                print(product)
                product_get[x]
                print(product_get[x])
                product['qty'] = product_get[x]
                totalsub = product['price'] * product_get[x]
                print(type(totalsub))
                product['sub_total'] = totalsub 
                products_list.append(product)
            print(products_list)
            print(type(products_list))
            sum_quantity = 0
            for item in products_list:
                mycnt = item['qty']
                sum_quantity += mycnt

            sumquantity =  sum_quantity
        
            context = {'sumquantity':sumquantity}    
            return render(request,'LoginView.html', context)
        else:
            context = {'sumquantity':0}
            return render(request,'LoginView.html', context)

    if request.method == 'POST':
        get_user = requests.get('http://192.168.1.25:5000/post-user/').json()
        user_get_load = json.loads(json.dumps(get_user))
        print(user_get_load)
        email = request.POST['email']
        password = request.POST['password']
        login = False
        for user in user_get_load:
            if(user['email'] == email and user['password'] == password):
                request.session['login']=user['id']
                login = True
                print('login')   
                break
            else:
                login = False
                print('dont login to page')   
        if login:
            print('user_id' + str(user['id']))
            user_id = user['id']
            pst = pytz.timezone('Asia/Ho_Chi_Minh')
            time_now=datetime.datetime.now(pst)
            time_access_now = time_now.strftime("%d-%m-%Y")
            print(time_access_now)
            month_number = time_now.strftime("%m-%Y")
            print(month_number)



            week_number= time_now.isocalendar()[1]
            print('date_access' + str(time_now))
            print('week number' + str(week_number))

            access={'user_id':user_id, 'date_access':time_access_now, 'week_access':week_number, 'month_access':month_number }
            headers = {'content-type': 'application/json'}
            r = requests.post('http://192.168.1.25:5000/statistical_user_visit/',headers =headers,data=json.dumps(access,indent=4, sort_keys=True, default=str))
            print('ok' + r.text)



            return redirect('/websitecosmetics/checkout/')
            
        else:
            messages.error(request, 'username or password false.')
            return redirect('/websitecosmetics/login/')
        
    else:
       
        return render(request,'LoginView.html')
            

