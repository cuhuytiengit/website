// var express = require("express");
// const cors = require("cors");
// const app = express();
// app.use(express.urlencoded({ extended: true }));
// app.use(cors());
// app.use((req, res, next) => {
//   res.setHeader("Access-Control-Allow-Origin", "*");
//   res.setHeader(
//     "Access-Control-Allow-Headers",
//     "Origin, X-Requested-With, Content-Type, Accept"
//   );
//   res.setHeader(
//     "Access-Control-Allow-Methods",
//     "GET, POST, PATCH, DELETE, OPTIONS"
//   );
//   next();
// });
// app.use(express.json());
// app.use("/api", jobRouter);
var http = require("http");

// Bước 2: Khởi tạo server
var server = http.createServer(function (request, response) {
  // Thiết lập Header
  response.setHeader(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  response.end();
});

// Bước 3: Lắng nghe cổng 300 thì thực hiện chương trình
server.listen(3000, function () {
  console.log("Connected Successfull!");
});
